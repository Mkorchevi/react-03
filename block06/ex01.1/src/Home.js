import React from 'react';
import { NavLink } from 'react-router-dom';
//import Navbar from './Navbar';

const Home = ()=>{
    let products = [
        {prodname: 'Black Shoes', id:10}, 
        {prodname: 'Brown Hat', id:11}, 
        {prodname: 'Pink Dress', id:12},
    ]
    let renderProd = products.map( (prod, idx)=>{
        return <NavLink key={idx} to={`/singleproduct/${prod.id}`}>
        <h1>{prod.prodname}</h1>
        </NavLink>
    } )

    return <div>

        {renderProd}
        
    </div> 
}



export default Home