import React from 'react';
import {NavLink} from 'react-router-dom';

const Navbar = (props)=>{
    return <div className='navbar'>

        <NavLink exact to={'/home'}>Home</NavLink>

        <NavLink exact to={'/about'}>About Us</NavLink>

        <NavLink exact to={'/contact'}>Contact Us</NavLink>

        <NavLink exact to={'/gallery'}>Gallery</NavLink>

    </div>
}

export default Navbar


 /*    <ul onClick={(e)=>props.selectPage(e.target.textContent)}>
            <li>Home</li>
            <li>About</li>
            <li>Contact</li>
            <li>Gallery</li>
</ul> */