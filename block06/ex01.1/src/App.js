import React from 'react';
import './App.css';
import Home from './Home';
import About from './About';
import Contact from './Contact';
import Gallery from './Gallery';
import Navbar from './Navbar'; 
import SingleProduct from './SingleProduct';

import {BrowserRouter as Router, Route} from 'react-router-dom';

class App extends React.Component{
 // state = {
  //  isLoggedIn: true,
  //}

  render(){
    return(
      <div className='App'>
      <Router>
        <Navbar />
        <Route exact path='/home' component={Home} />
        <Route exact path='/about' render= { (props)=> <About year={2021} {...props} />} />
        <Route exact path='/contact' component={Contact} />
        <Route exact path='/gallery' component={Gallery} />
        <Route exact path='/singleproduct/:product'component={SingleProduct} />
      </Router>

      <footer>Copywriter 2021 by Marcela k.</footer>
      </div>
    );
  }
}

export default App;

// el footer es estático para todas las paginas.

// <Route exact path='/contact' render={ props=>(!this.state.isLoggedIn ? 
// <Redirect to='/home'/> : <Contact { ...props }/> )} />
