import React from 'react';
import './App.css';
import Home from './Home';
import About from './About';
import Contact from './Contact';
import Gallery from './Gallery';
import Navbar from './Navbar'; 

class App extends React.Component{

   state = {
   page:'',
   }
 
   renderPages=(page)=>{
     this.setState({page}, ()=>{console.log(this.state)})
   }
 
   render(){
     return( <div className='App'>

      <Navbar selectPage={this.renderPages}/>
      {this.state.page === 'Home' ? 
      <Home /> : this.state.page === 'About' ? 
      <About /> : this.state.page === 'Contact' ? 
      <Contact /> : this.state.page === 'Gallery' ? 
      <Gallery /> : <Home />}
      <footer>Copywriter 2021 by Marcela k.</footer>
    </div>
    );
  }
}

export default App;

// el footer es estático para todas las paginas.
