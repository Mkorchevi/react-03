import React from 'react';

const Navbar = (props)=>{
    return <div className='navbar'>
        <ul onClick={(e)=>props.selectPage(e.target.textContent)}>
            <li>Home</li>
            <li>About</li>
            <li>Contact</li>
            <li>Gallery</li>
        </ul>
    </div>
}

export default Navbar