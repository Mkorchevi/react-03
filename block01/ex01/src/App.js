import React from 'react';
import './App.css';

function App() {
  let categories = ['t-shirts', 'hats', 'shorts', 'shirts', 'pants', 'shoes']

  let renderCategories = categories.map((ele, idx) => {
    return <p key={idx}>I need {ele}</p>
  })
  return (
    <div className="App">
      <h1>Clothes list</h1>
      {renderCategories}
    </div>
  );
}

export default App;
