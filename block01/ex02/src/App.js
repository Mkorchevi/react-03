import React from 'react';
import './App.css';

function App() {
  const products = [
    {
      product    : 'flash t-shirt',
      price      :  27.50,
      category   : 't-shirts',
      bestSeller :  false,
      image      : 'https://images-na.ssl-images-amazon.com/images/I/61ZipyCaAKL._AC_UX385_.jpg',
      onSale     :  true
    },
    {
      product    : 'batman t-shirt',
      price      :  22.50,
      category   : 't-shirts',
      bestSeller :  true,
      image      : 'https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png',
      onSale     :  false
    },
    {
      product    : 'superman hat',
      price      :  13.90,
      category   : 'hats',
      bestSeller :  true,
      image      : 'https://banner2.kisspng.com/20180429/rqe/kisspng-baseball-cap-superman-logo-batman-hat-5ae5ef317f8366.9727520615250184175223.jpg',
      onSale     :  false
    }
]

let renderProducts = (arr) => (
  arr.map( (item, idx) => {
    return <div key={idx}>
    <h3>{item.product}</h3>
    <img src={item.image} alt=""/>
    <p>Price: {item.price}</p>
    <p>Category: {item.category}</p>
    </div>
  })
)

let bsellers = products.map( (item,idx)=> {
  if(item.bestSeller == true) {
    console.log('item ==>',item)
     return  <div key={idx}>
     <h3>{item.product}</h3>
     <img src={item.image} alt=""/>
     <p>Price: {item.price}</p>
     <p>Category: {item.category}</p>
     </div>}
 } )
     


  return (
    <div className="App">
      <header>
        <span className='logo'>Chalkd</span>
        <nav>
        <a className="nav-link" href="#home">Home</a>
		   	<a className="nav-link" href="#shop">Shop</a>
		   	<a className="nav-link" href="#custom">Custom</a>
		   	<a className="nav-link" href="#corporate">Corporate</a>
        <a className="nav-link" href="#contact">Contact</a>
         </nav>
      </header>
     <div className="products">
     {
       renderProducts(products)
     }
     </div>
     <h2>Best Sellers</h2>
     <div className="bsellers">
     {
      bsellers
     }
     </div>
      <footer>
        <div className='footer'>
        <p>Chalk us out</p>
        <a className="link-footer" href="">Friend Us On Facebook</a>
        <a className="link-footer" href="">Testimonial</a>
        <a className="link-footer" href="">About</a>
        </div>
        <div className='footer'>
        <p>What Even More</p>
        <a className="link-footer" href="">Shopping Info</a>
        <a className="link-footer" href="">Get In Touch</a>
        </div>
      </footer>
    </div>
  );
}

export default App;
