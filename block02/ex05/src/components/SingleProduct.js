import React from 'react';

let SingleProduct = (props) => {
    console.log('props in SingleProduct.js ==>', props)

    return <div>
    <h3>{props.products.product}</h3>
    <img src={props.products.image} alt=""/>
    <p>Price: {props.products.price}</p>
    <p>Category: {props.products.category}</p>
    </div>
}

export default SingleProduct