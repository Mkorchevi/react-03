import React from 'react';
const Header =()=>{
    return <header>
    <h1>Chalkd</h1>
    <nav>
    <a className="nav-link" href="#home">Home</a>
           <a className="nav-link" href="#shop">Shop</a>
           <a className="nav-link" href="#custom">Custom</a>
           <a className="nav-link" href="#corporate">Corporate</a>
    <a className="nav-link" href="#contact">Contact</a>
     </nav>
  </header>
}

export default Header