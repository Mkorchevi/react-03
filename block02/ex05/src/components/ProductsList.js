import React from 'react';
import SingleProduct from './SingleProduct';

let ProductsList =(props)=>{
    console.log('props in ProductsList.js ==>', props)

    let renderProducts = (arr) => (
        arr.map( (item, idx) => {
          return <SingleProduct products={item} key={idx}/>
        })
      )
return <div>{renderProducts(props.products)}</div>
}

export default ProductsList