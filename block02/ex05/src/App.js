import './App.css';
import Footer from './components/Footer';
import Header from './components/Header';
import ProductsList from './components/ProductsList';

function App() {
  const products = [
    {
      product    : 'flash t-shirt',
      price      :  27.50,
      category   : 't-shirts',
      bestSeller :  false,
      image      : 'https://images-na.ssl-images-amazon.com/images/I/61ZipyCaAKL._AC_UX385_.jpg',
      onSale     :  true
    },
    {
      product    : 'batman t-shirt',
      price      :  22.50,
      category   : 't-shirts',
      bestSeller :  true,
      image      : 'https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png',
      onSale     :  false
    },
    {
      product    : 'superman hat',
      price      :  13.90,
      category   : 'hats',
      bestSeller :  true,
      image      : 'https://banner2.kisspng.com/20180429/rqe/kisspng-baseball-cap-superman-logo-batman-hat-5ae5ef317f8366.9727520615250184175223.jpg',
      onSale     :  false
    }
]


let bsellers = products.map( (item,idx)=> {
  if(item.bestSeller == true) {
    console.log('item ==>',item)
     return  <div key={idx}>
     <h3>{item.product}</h3>
     <img src={item.image} alt=""/>
     <p>Price: {item.price}</p>
     <p>Category: {item.category}</p>
     </div>}
 } )

  return (
    <div className="App">
      
      <Header />

      <ProductsList products= {products}/>


     <h2>Best Sellers</h2>

     {
      bsellers
     }

     <Footer />
    </div>
  );
}

export default App;
