import React from 'react'
import Item from './Item.js'

class List extends React.Component {
     render () {
     return ( this.props.fruits.map(ele => <Item key={ele} fru={ele}/>)
          );
     }
}


export default List;