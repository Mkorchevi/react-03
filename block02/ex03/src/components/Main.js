import React from 'react'
import List from './List.js'

class Main extends React.Component {
  render () {
    const fruits = ['banana','kiwi','orange','mango']
  return ( <div className="Main">
   <List fruits={fruits}/> 
  </div>
    );
  }
}

  export default Main;