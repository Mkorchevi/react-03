import React from 'react';
import './App.css';
import Child from "./components/Childone";
import Child2 from "./components/Childtwo";

class App extends React.Component {
  render() {
  const firstnames = ['Tom', 'Daniel', 'Harley', 'Sarah', 'Paul'];
  const lastnames= ['Hardy', 'Craig', 'Atwell', 'Brightman', 'Bettany'];

  return (
    <div className = 'fullname'>
    
      <Child firstnames={firstnames} /> 
     
      <Child2 lastnames={lastnames} />
    </div>
     );
   }
  }

export default App;

// <h2>First Name</h2>
// <h2>Last Name</h2>

 // let renderFirstnames = (firstnames) => (
 //   firstnames.map((ele,i)=>{
 //     return <h1 key={i}>{ele}</h1>
 //   })
 // )

 // let renderLastnames = (lastnames) => (
 //   lastnames.map((ele,i)=>{
 //     return <h1 key={i}>{ele}</h1>
 //   })
 // )

 
 // return (
  //  <div className="App">

    //  {renderFirstnames(firstnames)} 
   //   {renderLastnames(lastnames)}   
  //  </div>
 // );
//}