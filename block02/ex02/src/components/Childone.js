import React from "react";

class Child extends React.Component {
  render() {
    console.log(this.props);
    return (
      <div>
        {this.props.firstnames.map((ele, i) => (
          <p key={i}>{ele}</p>
        ))}
      </div>
    );
  }
}

export default Child;
