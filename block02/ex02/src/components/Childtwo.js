import React from "react";

class Child2 extends React.Component {
  render() {
    console.log(this.props);
    return (
      <div>
        {this.props.lastnames.map((ele, i) => (
          <p key={i}>{ele}</p>
        ))}
      </div>
    );
  }
}
export default Child2;
