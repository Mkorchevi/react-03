import React from 'react';

class Child extends React.Component {
    render() {
      console.log(this.props);
      return (
        <div>
          {this.props.greetings.map((ele, i) => (
            <h1 key={i}>Hello {ele} , I am  the child component!</h1>
          ))}
        </div>
      );
    }
  }


//const Child = props => {
  //  console.log(props);
 //   let {greetings} = props;
 //   return (
//        <h1>
 //           Hello {props.greetings}, I am  the child component!
 //       </h1>
 //   );
// };

export default Child;