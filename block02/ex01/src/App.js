import React from 'react';
import './App.css';
import Child from './components/Child';


class App extends React.Component {
  render() {
    const greetings = ['Marcela'];
    return (
      <div>
        <Child greetings ={greetings} />
      </div>
    );
  }
}




//const App = () => {
//  const greetings = 'Marcela';
//  return (
 //   <div className="App">
   //   <Child greetings ={greetings} />
  //  </div>
 // );
//};

export default App;
