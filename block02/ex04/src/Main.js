import List from './List.js'
function Main() {
    const fruits = ['banana','kiwi','orange','mango']
    return (
      <div className="Main">
         <List fruits={fruits}/> 
      </div>
    );
  }
  export default Main;