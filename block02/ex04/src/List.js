import Item from './Item.js'
function List(props) {
     return props.fruits.map( ele => <Item key={ele} fru={ele}/>)
}
export default List;