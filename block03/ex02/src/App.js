import React, {useState} from 'react';
import './App.css';

function App () {
  const [fname, setFname] = useState('');
  const [lname, setLname] = useState('');

  const handleFnameInput = e =>setFname (e.target.value);
  const handleLnameInput = e =>setLname (e.target.value);

  const submitInput = () => {
    if( !fname || !lname){
      return alert('invalid data provided')
    }else{
      return alert(`Hello ${fname} ${lname}`)
    }
  }
    
    return ( <div className='App'>
    <header>
      <h1>Please completed this from</h1>
      
      <p>Firtsname</p>
      <input onChange={handleFnameInput}/>
      <p>Lastname</p>
      <input onChange={handleLnameInput}/>

        <p>Click on Submit button to send</p>
       <button onClick={submitInput}>Submit</button>

      <h1>Your fullname is: {fname + ' ' + lname}</h1>
    </header>
    </div>
   );
}

export default App;
