import React from 'react';
import './App.css';

class App extends React.Component {
  state = {
    fname: '',
    lname: '',
    fullName: '',
  }

  handleChange =(event)=>{
    let data = event.target.value
    this.setState({[event.target.name]:data}, ()=>{console.log('state==>', this.state)})
  }

  submitInput =()=>{
    let {fname,lname} = this.state;
    let full = `${fname} ${lname}`;
    alert(this.state.fname === '' || this.state.lname === '' ? 'No data provided!': `Hello ${full}`)
    this.setState({fullName: full, lname:'', fname:''})
  }


  render() {

    return ( <div className='App'>
    <header>
      <h1>Please completed this from</h1>

      <p>Firtsname</p>
      <input onChange={this.handleChange} name='fname' value={this.state.fname}/>
      <p>Lastname</p>
      <input onChange={this.handleChange} name='lname' value={this.state.lname}/>

      <p>Click on Submit button to send</p>
      <button onClick={this.submitInput}>Submit</button>

      <h1>Your fullname is: {this.state.fullName}</h1>
    </header>
    </div>)
  }
}

export default App;
