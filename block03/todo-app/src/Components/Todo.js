import React from 'react';

function Todo({ todo, index, completeTodo, removeTodo }) {
    return (<div className="todo">
        <div className='items' style={{ textDecoration: todo.completed ? "line-through" : '' }}>{todo.text}</div>
        <div className='buttons'>
          <button className='button-one' onClick={() => completeTodo(index)}>✔</button>
          <button className='button-two' onClick={() => removeTodo(index)}>✘</button>
        </div>
      </div>
    );
  }


export default Todo;