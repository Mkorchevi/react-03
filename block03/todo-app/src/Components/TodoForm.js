import React, {useState} from 'react';

function TodoForm({ addTodo }) {
    const [value, setValue] = useState('');
  
    const handleSubmit = e => {
      e.preventDefault();
      if (!value) return;
      addTodo(value);
      setValue("");
    };
  
    return (<form className='submit' onSubmit={handleSubmit}>
        <input placeholder="Add your to do here"
          type="text"
          className="input"
          value={value}
          onChange={e => setValue(e.target.value)}
        />
        <button type="submit">Submit</button>
      </form>
    );
  }

export default TodoForm;