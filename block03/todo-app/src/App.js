import React, {useState} from "react";
import "./App.css";
import TodoForm from "./Components/TodoForm";
import Todo from "./Components/Todo";

function App() {
  const [todos, setTodos] = useState([
    { text: 'Item 1', completed: false },
    { text: 'Item 2', completed: false },
    { text:'Item 3', completed: false }
  ]);

  const addTodo = text => {
    const newTodos = [...todos, { text }];
    setTodos(newTodos);
  };

  const completeTodo = index => {
    const newTodos = [...todos];
    newTodos[index].completed = true;
    setTodos(newTodos);
  };

  const removeTodo = index => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
  };

  return (<div className='App'>
          <h1>Todo App</h1>
          <TodoForm addTodo={addTodo} />

        {todos.map((todo, index) => (
          <Todo
            key={index}
            index={index}
            todo={todo}
            completeTodo={completeTodo}
            removeTodo={removeTodo}
          />
        ))} 
    </div>
  );
}


export default App;