import React from 'react';
import './App.css';
import Input1 from './Components/Inputone';
import Input2 from './Components/Inputtwo';
import Button from './Components/Button';


class App extends React.Component {
  state = {
    email: '',
    password: '',
  }

  handleChange = (event) => {
    let data = event.target.value
    this.setState({[event.target.name]:data}, ()=>{console.log('state==>', this.state)})
  }

  submitInput = () => {
    const email = this.state.email;
    const password = this.state.password;
    alert(`Hello ${email} ${password}`)
  }

  render() {
    return (    
    <div className='userinputs'>
      <h1>Please completed this from</h1>
      
      <p>Firtsname</p>
      <Input1 handleChange={this.handleChange} email={this.state.email}/>
      
      <p>Lastname</p>
      <Input2 handleChange={this.handleChange} password={this.state.password}/>
      
      <p>Click on Submit button to send</p>
      <Button submitInput={this.submitInput}/>
    </div>)
  }
}

export default App;
