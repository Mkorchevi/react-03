import React from 'react';

class Button extends React.Component {  
    render() {
        return (
            <button onClick={this.props.submitInput}>Submit</button>
          );
    }
}

export default Button;