import React from 'react';

class Input2 extends React.Component {  

    render() {
        return (
            <input onChange={this.props.handleChange} name='password' value={this.props.password}/>
          );
    }
}

export default Input2;