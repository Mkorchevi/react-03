import React, {useContext} from 'react'
import { SomeContext } from './Context'

const SecondChild = () => {
    const value = useContext(SomeContext);
    return <h1 style={{color:value}}>{value}</h1>
  }
  
export default SecondChild;