import React, { useState, useEffect } from 'react';
import './App.css';
import { SomeContext } from './Context';
import Child from './Child';

const App = () => {
    const username = [
      { email:'marina@gmail.com', password:'1234mar'},
      { email:'pedro@gmail.com', password:'p23458'},
      { email:'jose@gmail.com', password:'958j2'},
      { email:'sonia@gmail.com', password:'123456'},
      { email:'luis@gmail.com', password:'78510'},
      { email:'test', password:'pass'}
    ]
    const [color,setColor] = useState(null)
    const [message, setMessage] = useState('')

   // setTimeout(function() {
    //  let that = this;
    //  that.setState(()=> );
   // }, 1000);
 // }
//  useEffect(() => {
//   const interval = setInterval(() => {
//     console.log(`This message will disappear in ${3}`);
//   }, 1000);
//   return () => clearInterval(interval);
// }, []);

    const [email, setEmail] = useState('');
    const [password,  setPassword ] = useState('');
  
    const handleNameChange = (e) => setEmail(e.target.value)
    const handlePasswordChange = (e) => setPassword(e.target.value)


   return (<div className='App'>
             <Child/>
         <form onSubmit={event => {
                  event.preventDefault();
                  //! check here if email and password are inside username
                  const index = username.findIndex(ele => ele.email === email && ele.password === password)
                  if( index === -1 ){
                      setColor('red')
                  }else{
                      setColor('green')
                  }
                  let counter = 3;
                  const interval = setInterval(() => {
                    console.log(`This message will disappear in ${counter}`);
                    setMessage(`This message will disappear in ${counter}`)
                    counter--
                    if(counter < 0) {
                      setMessage('')
                      return clearInterval(interval)
                    }
                  }, 1000);
               }}>
  
            <div>Email: 
                <input onChange={handleNameChange}/>
                <p>{email}</p>
            </div>
  
            <div>Password:
                <input onChange={handlePasswordChange}/>
                <p>{password}</p>
            </div>
            <button>SUBMIT</button>
         </form>
         <h2 style={{color:color}}>{message}</h2>
      </div>
    );
}
export default App;