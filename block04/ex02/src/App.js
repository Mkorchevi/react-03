import React, {useState} from 'react';
import './App.css';
import Input from './Input';

function App() {
	const [ euro, setEuro ] = useState(true);
	const [ initial, setInitial ] = useState(0);

	const handleChange = (type, value) => setInitial(value);

	return (
		<div className="App">
			<h1>Convert Euro to Dollar and viceversa</h1>
			<Input type="dollar" label="Convert: " handleChange={handleChange} />
			<button className="button" onClick={() => setEuro(!euro)}>{euro ? 'EUR - USD' : 'USD - EUR'}</button>
			<h1>{!euro ? initial * 1.1 : initial * 0.9}</h1>
		</div>
	);
}

export default App;

