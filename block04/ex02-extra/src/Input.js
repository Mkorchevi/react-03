import React from 'react';

function Input(props) {
	const { label, type, handleChange } = props;
	const onChange = (e) => {
		handleChange(type, e.target.value);
	};
	return (
		<div>
			<label>{label}</label>
			<input onChange={onChange} type="number" />
		</div>
	);
}

export default Input;
