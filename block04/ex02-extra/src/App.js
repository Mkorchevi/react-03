import axios from 'axios';
import React, {useState, useEffect} from 'react';
import './App.css';
import Input from './Input';

function App() {
  const [euro, setEuro] = useState(true);
  const [initial, setInitial] = useState(0);
  const [currency, setCurrency] = useState(0);

  useEffect(() => {
		const changeCurrency = () => {
			let url = `http://apilayer.net/api/live?c=&access_key=0249cb1d2e97cff93d2a726ba3f60e0a`;
			axios
				.get(url)
				.then((response) => {
					console.log('response ===>',response.data.quotes.USDEUR);
					setCurrency(response.data.quotes.USDEUR);
				})
				.catch((e) => {
					console.log('Error:', e);
				});
		};
		changeCurrency()
  },[])
  const handleChange = (type, value)=> setInitial(value)
  
  return ( <div className="App">
    <h1>Convert Euro to Dollar and viceversa</h1>
    <Input type="dollar" label="Convert: " handleChange={handleChange} />
	<button className="button" onClick={() => setEuro(!euro)}>{euro ? 'EUR - USD' : 'USD - EUR'}</button>
	<h2>{!euro ? initial * currency : initial / currency}</h2>
    </div>
  );
}

export default App;
