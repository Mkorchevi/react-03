import React from 'react';
import './App.css';
import Counter from './Component/Counter';

class App extends React.Component {
	state = {
		count: 1
	};
	clicked = () => {
		let setCount = this.state.count + 1;
		this.setState({ count: setCount }, () => {
			console.log('click', this.state);
		});
	};
	render() {
		return ( <div className="App">
				<h1>Show only odd clicked{' '}
					{this.state.count % 2 === 0 && this.state.count !== 0 ? (
						this.state.count - 1
					) : (
						this.state.count
					)}{' '}
					times
				</h1>
				<Counter clicked={this.clicked} />
			</div>
		);
	}
}

export default App;

